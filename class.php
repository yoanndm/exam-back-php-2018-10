<?PHP

class Users
{
	protected $first_name;
	protected $last_name;
	protected $email;
	protected $created_at;
	protected $updated_at;
	protected $password;

	public function __construct()
	{
		try {
			// Successful database connection
			$db = new PDO('mysql:host='.$_ENV['DB_HOST_LOCAL'].';dbname='.$_ENV['DB_NAME'], $_ENV['DB_USER_LOCAL'], $_ENV['DB_PSSW_LOCAL']);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db = $db;

			$selectUsers = $this->db->query('SELECT * FROM users;');
			$this->selectUsers = $selectUsers->fetchAll(PDO::FETCH_OBJ);
		} catch(Exception $e) {
			// Database connection failed
			throw new Exception($e->getMessage());
		}
	}


	public function getFirstName()
	{
		$sql = 'SELECT * FROM users';
		foreach ($this->db->query($sql) as $row)
		{
			$this->first_name = $row['first_name'];
		}

		return $this->first_name;
	}

	public function setFirstName($first_name)
	{
		$this->first_name = $first_name;
		return $this;
	}

	public function getLastName()
	{
		$sql = 'SELECT * FROM users';
		foreach ($this->db->query($sql) as $row)
		{
			$this->last_name = $row['last_name'];
		}

		return $this->last_name;
	}

	public function setLastName($last_name)
	{
		$this->last_name = $last_name;
		return $this;
	}

	public function getFullName()
	{
		return $this->getFirstName() . ' ' . $this->getLastName();
	}

	public function getEmail()
	{
		$sql = 'SELECT * FROM users';
		foreach ($this->db->query($sql) as $row)
		{
			$this->email = $row['email'];
		}

		return strtolower($this->email);
	}

	public function setEmail($email)
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return;
		}

		$this->email = $email;
		return $this;
	}

	public function getCreatedAt()
	{
		$sql = 'SELECT * FROM users';
		foreach ($this->db->query($sql) as $row)
		{
			$this->created_at = $row['created_at'];
		}

		return $this->$created_at;
	}

	public function setCreatedAt($created_at)
	{
		$this->$created_at = $created_at;
		
	}

	public function getUpdatedAt()
	{
		$sql = 'SELECT * FROM users';
		foreach ($this->db->query($sql) as $row)
		{
			$this->updated_at = $row['updated_at'];
		}

		return $this->$updated_at;
	}

	public function setUpdatedAt($updated_at)
	{
		$this->$updated_at = $updated_at;
		return $this;
	}

	public function getPassword()
	{
		$sql = 'SELECT * FROM users';
		foreach ($this->db->query($sql) as $row)
		{
			$this->password = $row['password'];
		}

		return $this->$password;
	}

	public function setPassword($password)
	{
		$this->$password = $password;
		return $this;
	}
}