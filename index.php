<?php
// modules composer
require __DIR__ . '/App/vendor/autoload.php';

// Module Dotenv
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();	

// Module Whoops
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

try {
	// Successful database connection
	$db = new PDO('mysql:host='.$_ENV['DB_HOST_LOCAL'].';dbname='.$_ENV['DB_NAME'], $_ENV['DB_USER_LOCAL'], $_ENV['DB_PSSW_LOCAL']);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(Exception $e) {
	// Database connection failed
	throw new Exception($e->getMessage());
}

require('class.php');

$selectUsers = $db->query('SELECT * FROM users;');
$selectUsers = $selectUsers->fetchAll(PDO::FETCH_OBJ);

// print_r($selectUsers);

$users = new Users;
echo('<ul>');

foreach ($selectUsers as $user) {
	echo '<li>' . $users->getFirstName() . $users->getEmail() . $users->getPassword() . '</li>';
}
// or ..
// foreach ($selectUsers as $user) {
	// $users->setFirstName($user->first_name);
	// $users->setLastName($user->last_name);
	// $users->setEmail($user->email);
	// $users->setCreatedAt($user->created_at);
	// $users->setUpdatedAt($user->updated_at);
	// $users->setPassword($user->password);

	// echo '<li>' . $users->getFirstName() . $users->getEmail() . $users->getCreatedAt() . $users->getUpdatedAt() . $users->getPassword() . '</li>';
// }
echo('</ul>');